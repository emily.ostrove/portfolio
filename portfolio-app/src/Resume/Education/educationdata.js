const degrees = [
    {
        degree: 'Software Engineering Coding Bootcamp with JavaScript and Python',
        school: 'Hack Reactor',
        link: 'https://www.hackreactor.com/',
        year: 2022,
    },
    {
        degree: 'B.S. Biochemistry, Biophysics, and Molecular Biology',
        school: 'Whitman College',
        link: 'https://www.whitman.edu/',
        year: 2015,
    }
]

export default degrees;