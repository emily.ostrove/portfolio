const languages = [
    {
        language: 'English',
        proficiency: 'Native',
    },
    {
        language: 'German',
        proficiency: 'Bilingual',
    },
    {
        language: 'Spanish',
        proficiency: 'Basic to intermediate',
    }
]

export default languages;